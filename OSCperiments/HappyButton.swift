//
//  HappyButton.swift
//  OSCperiments
//
//  Created by Gian on 24/10/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import SwiftUI
import SwiftOSC

struct HappyButton: View {
    
    var ID: Int?
    
    var client = OSCClient(address: "255.255.255.255", port: 8100)
    
    var body: some View {
        
        Button(action: {self.bro()}) {
            Image("SmileyButton")
                .renderingMode(.original)
                .resizable()
                .frame(width: 100, height: 100)
        }
        .frame(width: 100, height: 100)
        
    }
    
    
    func bro(){
        //print("bro \(ID ?? 666)")
        let message = OSCMessage(OSCAddressPattern ("/botonet\(ID ?? 666)"), 1)
        client.send(message)
    }
}


struct HappyButton_Previews: PreviewProvider {
    static var previews: some View {
        HappyButton()
    }
}

