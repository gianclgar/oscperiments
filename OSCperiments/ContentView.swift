//
//  ContentView.swift
//  OSCperiments
//
//  Created by Gian on 12/10/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import SwiftUI
import SwiftOSC

var client = OSCClient(address: "255.255.255.255", port: OUTport)
var OUTport = 8000
var boletesnum = 3

struct ContentView: View {
    var body: some View {
        VStack {
            
            VStack {
                Spacer()
                HStack{
                HappyButton(ID: 1)
                HappyButton(ID: 2)
                HappyButton(ID: 3)
                }
                HStack{
                HappyButton(ID: 4)
                HappyButton(ID: 5)
                HappyButton(ID: 6)
                }
                
                CustomSlider1()
                CustomSlider1()
                CustomSlider1()
                SuperCustomButton1()
             
                Spacer()
                
                
                /*
                Button(action: {self.bro()}) {
                        Image("SmileyButton")
                            .renderingMode(.original)
                            .resizable()
                            .frame(width: 100, height: 100)
                        }
                .frame(width: 100, height: 100)
                .accentColor(.green)
                Button(action: {}) {
                        Image("SmileyButton")
                            .renderingMode(.original)
                            .resizable()
                            .frame(width: 100, height: 100)
                        }
                .frame(width: 100, height: 100)
                .accentColor(.green)
                
                */
            }
            
            Button(action: {
                if let url = URL(string: "http://dwww.flaticon.com/authors/iconixar"){
                    UIApplication.shared.open(url)
                }
            }) {
                Text("AppIcon by iconixar from flaticon.com")
                    .font(.footnote)
                   
            }
        }
    }
    
    func bro(){
        print("bro")
    }
        
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
