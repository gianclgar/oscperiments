//
//  CustomSlider1.swift
//  OSCperiments
//
//  Created by Gian on 15/10/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import SwiftUI
import SwiftOSC

struct CustomSlider1: View {
    
    var client = OSCClient(address: "255.255.255.255", port: 8100)
    var ShowValue = false

    
    @State var progress: Double = 0.5
    
    var body: some View {
        
        
        return VStack{
           Slider(value: Binding(get: {
               self.progress
           }, set: { (newVal) in
               self.progress = newVal
               self.sliderchanged()
           }))
           .padding(.all)
           
            Text(String(progress))
        }
    }


    
    func sliderchanged() {
        let message = OSCMessage(OSCAddressPattern ("/whatever"), "hey sliderchanged \(progress)")
        client.send(message)
        print("Slider value changed to \(progress)")
    }
}

struct CustomSlider1_Previews: PreviewProvider {
    static var previews: some View {
        CustomSlider1()
    }
}
