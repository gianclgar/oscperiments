//
//  SuperCustomButton1.swift
//  OSCperiments
//
//  Created by Gian on 21/10/2019.
//  Copyright © 2019 Giannino Clemente. All rights reserved.
//

import SwiftUI
import SwiftOSC

struct SuperCustomButton1: View {
    
   var client = OSCClient(address: "255.255.255.255", port: 8100)
    
   @State private var idx = 0
    
    var body: some View {
        
        var isPressed = 0
//        var butcolor: Color = .black
        let g = DragGesture(minimumDistance: 0, coordinateSpace: .local).onChanged({_ in
           
            
            
            if isPressed == 0 {
            isPressed = 1
            let message = OSCMessage(OSCAddressPattern ("/supercustom"), 1)
            self.client.send(message)
//            butcolor = .red
            
//            print(isPressed)
            }
            
        }).onEnded({_ in
            
            isPressed = 0
            let message = OSCMessage(OSCAddressPattern ("/supercustom"), 0)
            self.client.send(message)
//            butcolor = .black
            
//            print(isPressed)
            
        })

        return VStack {
            Circle().frame(width: 100, height: 100).gesture(g).accentColor(.red)
//            Text(String(isPressed))
        }
    }
}

struct SuperCustomButton1_Previews: PreviewProvider {
    static var previews: some View {
        SuperCustomButton1()
    }
}
